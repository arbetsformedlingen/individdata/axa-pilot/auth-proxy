import express, { Application } from "express";
import pino from "pino";
import httpPino from "pino-http";
import session from "express-session";
import { csrfSync } from "csrf-sync";
import { z } from "zod";

declare module "express-session" {
  interface SessionData {
    ssn: string;
  }
}

const logger = pino({ mixin: () => ({ logger: "auth" }) });

const app: Application = express();
const port = process.env.PORT || 8000;

app.use(httpPino(logger));
app.use(express.urlencoded({ extended: false }));
app.use(
  session({
    secret: "keyboard cat",
    name: "fakeauth",
    resave: false,
    saveUninitialized: false,
  })
);

app.set("view engine", "ejs");

const parseSSN = (s: string) => {
  if (/(19|20)\d{6}-?\d{4}/.test(s)) {
    return s.replace("-", "");
  }

  throw new Error("Failed to parse SSN.");
};

// Configure list of SSNs from environment variable SSN_LIST (comma separated)
const defaultSsnList: string[] = [];
const clients: Record<string, { ssnList: string[] }> = {};
let clientId;

for (let entry of process.env.SSN_LIST.split(",")) {
  const parsed = z.string().uuid().safeParse(entry);
  const ssn = z
    .string()
    .regex(/^\d{12}$/)
    .safeParse(entry);
  if (parsed.success) {
    clientId = parsed.data;
    clients[clientId] = { ssnList: [] };
  } else if (ssn.success) {
    if (clientId !== undefined) {
      clients[clientId].ssnList.push(ssn.data);
    } else {
      defaultSsnList.push(ssn.data);
    }
  }
}

const ssnList = (process.env.SSN_LIST || "")
  .split(",")
  .map((s) => {
    try {
      return parseSSN(s);
    } catch {
      return null;
    }
  })
  .filter((item) => item !== null);

const { csrfSynchronisedProtection, generateToken } = csrfSync({
  getTokenFromRequest: (req) => {
    return req.body._csrf;
  }, // Used to retrieve the token submitted by the user in a form
});

const pickSsnList = (url?: string) => {
  if (url === undefined) return defaultSsnList;
  const params = new URLSearchParams(url.substring(2));
  const clientId = params.get("clientId");
  if (clientId === undefined) return defaultSsnList;
  const client = clients[clientId];
  if (client === undefined) return defaultSsnList;
  const { ssnList } = client;
  return ssnList;
};

app.get("/_/login", async (req, res) => {
  const csrfToken = generateToken(req);
  const redirectURL = req.query.url;
  const error = req.query.error;

  const ssnList = pickSsnList(redirectURL as string);

  res.render("login", { ssnList, csrfToken, redirectURL, error });
});

app.post("/_/login", csrfSynchronisedProtection, async (req, res) => {
  if (!req.body.url) {
    req.log.warn("No redirect URL provided, using default (/).");
  }
  const redirectURL = req.body.url && req.body.url !== "" ? req.body.url : "/";

  req.log.debug({ ssn: req.body.ssn, redirectURL });

  if (req.body.ssn && ssnList.includes(req.body.ssn)) {
    req.session.ssn = req.body.ssn;

    return res.redirect(302, redirectURL);
  }

  return res.redirect(302, `/_/login?url=${req.body.url}&error=invalid`);
});

app.get("/_/logout", async (req, res) => {
  req.session.ssn = null;
  await req.session.save();

  res.redirect("/");
});

app.get("/_/verify", (req, res) => {
  if (!req.session.ssn) {
    const returnURL = encodeURIComponent(
      req.headers["x-forwarded-uri"] as string
    );
    return res.redirect(302, `/_/login/?url=${returnURL}`);
  }

  req.log.info({ session: req.session });

  res.header("Remote-User", req.session.ssn);
  res.status(200);
  res.end();
});

if (ssnList.length > 0) {
  app.listen(port, () => {
    logger.info(`Server is running at port ${port}.`);
    logger.info({ ssnList }, `Using ${ssnList.length} SSNs.`);
  });
} else {
  logger.error(`No SSN_LIST provided, exiting ...`);
  process.exitCode = 1;
}
