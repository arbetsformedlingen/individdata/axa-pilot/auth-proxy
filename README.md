# Fake Authenticator

This is a small application for use in development. It is intended to be used
as a replacement to a proper authenticated proxy during development.

The fake authenticator has two endpoints `/_/login` and `/_/verify`. The
reverse proxy that wants to use the fake authenticator calls the `/_/verify`
endpoint. If the user is authenticated it responds with http status 200, or
else responds with a redirect (302) to `/_/login` which will initiate the sign
in procedure.

## Usage

Build the container:

```shell
podman build -t fakeauth:latest .
```

If a certain set of personal numbers are desired when building they can be
provided through `--build-arg SSN_LIST=<ssns>` where `<ssns>` are a comma
separated list of 12 digit personal numbers.

Once built, the container image can be started with:

```shell
podman run -p 4000:4000 -e PROXY_TARGET=frontend:3000 fakeauth:latest
```

Where the `PROXY_TARGET` environment variable is where the protected
application resides. It has to be reachable by the container.

### Personal identification numbers

The container is built with a default list of personal identification numbers
that are available in the UI. This list can be overridden by providing a comma
separated list of values as the environment variable `SSN_LIST`.

Some values that might be of use:

| PIN / SSN    | Comment |
|--------------|---------|
| 199010102383 | default |
| 199008272396 | default |
| 199812062397 | default |
| 198812192923 | default |
| 198708259216 |         |
| 192001059266 |         |
| 193001017072 |         |
| 193001034663 |         |
| 193001049752 |         |
| 193001051352 |         |
| 195006041239 |         |
| 195006142524 |         |
| 195006262546 |         |
| 195007070005 |         |
| 195007191116 |         |
| 195007282162 |         |
| 195007312589 |         |
| 195008072307 |         |
| 195008121856 |         |

## Running Caddy locally

To run Caddy locally with the provided example configuration run `caddy run`.
This will run the Caddy server on port 4000 and proxy the service on port
3000 while authenticating with the service on port 8000.

## Anatomy of container

The container uses Supervisor to manage the two processes of the node
application and the Caddy server. The motivation behind this is to mimic the
production system as closely as possible.
