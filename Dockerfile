# Install dependencies
FROM node:20.18.1-alpine3.19 as build

RUN apk add --no-cache git &&\
    npm upgrade -g npm

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm clean-install

COPY . .
RUN npm run build

# Production image
FROM node:20.18.1-alpine3.19

ARG SSN_LIST=199010102383,199008272396,199812062397,198812192923

RUN apk add --no-cache supervisor caddy &&\
    mkdir -p /var/run/supervisord
COPY supervisord.conf /etc/supervisord/conf.d/supervisord.conf
COPY Caddyfile /etc/caddy/Caddyfile

WORKDIR /app

COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/dist .
COPY src/views/ ./views/


EXPOSE 4000

ENV SSN_LIST=${SSN_LIST}\
    PROXY_TARGET=frontend:3000

CMD [ "/usr/bin/supervisord", "-c", "/etc/supervisord/conf.d/supervisord.conf" ]
